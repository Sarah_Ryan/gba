
#include <gba_console.h>
#include <gba_video.h>
#include <gba_interrupt.h>
#include <gba_systemcalls.h>

#include <stdio.h>

//---------------------------------------------------------------------------------
// Program entry point
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------

	// the vblank interrupt must be enabled for VBlankIntrWait() to work
	irqInit();
	irqEnable(IRQ_VBLANK);

	// generic setup function
	consoleDemoInit();
	
	unsigned short* DISPCNT = (unsigned short*)0x04000000;
	
	DISPCNT[0] = ((0 << 0 ) | (0 << 6) | (1 << 8) | (1 << 9) | (1 << 10) | (1 << 11) | (0 << 12));
	
	unsigned short* BGCNT = (unsigned short*)0x04000008;
	
	BGCNT[0] = ((2 << 0) | (0 << 2) | (0 << 7) | (11 << 8) | (0 << 14)) ;
	BGCNT[1] = ((3 << 0) | (1 << 2) | (0 << 7) | (10 << 8) | (0 << 14));
	BGCNT[2] = ((0 << 0) | (2 << 2) | (0 << 7) | (9 << 8) | (0 << 14)) ;
	BGCNT[3] = ((1 << 0) | (3 << 2) | (0 << 7) | (8 << 8) |(0 << 14));
	
	unsigned int* chars = (unsigned int*)0x6000000;
	
	chars[0] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[1] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[2] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[3] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[4] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[5] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[6] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[7] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	
	//Defines character base block location
	unsigned int* mychar = (unsigned int*)0x6000020;
	mychar[0] = ((0<<0)|(0<<4)|(0<<8)|(1<<12)|(1<<16)|(1<<20)|(0<<24)|(1<<28));
	mychar[1] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(1<<20)|(0<<24)|(1<<28));
	mychar[2] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(0<<24)|(1<<28));
	mychar[3] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(1<<24)|(1<<28));
	mychar[4] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(1<<24)|(1<<28));
	mychar[5] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(0<<24)|(1<<28));
	mychar[6] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(0<<24)|(1<<28));
	mychar[7] = ((0<<0)|(0<<4)|(1<<8)|(1<<12)|(1<<16)|(1<<20)|(0<<24)|(0<<28));
	
	//BG layer defined
	unsigned short* baseBlockAddress = (unsigned short*)0x6004000;
	baseBlockAddress[5 * 32 + 5] = ((1 << 0) | (15 << 12));

	// main loop
	while (1) 
	{
		// ansi escape sequence to clear screen and home cursor
		// /x1b[line;columnH
		iprintf("\x1b");

		// ansi escape sequence to set print co-ordinates
		// /x1b[line;columnH
		iprintf("\x1b[10;10HWEEK 1 EXAMPLE");



	
		VBlankIntrWait();
	}
}


