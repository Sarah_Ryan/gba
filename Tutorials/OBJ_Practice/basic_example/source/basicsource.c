
#include <gba_console.h>
#include <gba_video.h>
#include <gba_interrupt.h>
#include <gba_systemcalls.h>

#include <stdio.h>

//---------------------------------------------------------------------------------
// Program entry point
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------

	// the vblank interrupt must be enabled for VBlankIntrWait() to work
	irqInit();
	irqEnable(IRQ_VBLANK);

	// generic setup function
	consoleDemoInit();
	
	//Display control register pointer
	unsigned short* DISPCNT = (unsigned short*)0x04000000;
	
	//DISPCNT setup
	//bit 6 is for 1D OBJ data, 8 through 11 are the 4 layers
	//Bit 12 is the OBJ on/off switch
	DISPCNT[0] = ((1 << 6) | (1 << 8) | (1 << 9) | (1 <<10) | (1 << 11) | (1 << 12));
	
	//Pointer for the 4 BG layers
	unsigned short* BGCNT = (unsigned short*)0x04000008;
	
	//Bits 0 - 1 are the layer priority
	// Bits 2 - 3 are the character base block number
	//Bit 7 is colour mode
	//Bits 8 - 12 is the screen base block yo use for the layer
	//Bits 14 - 15 are used for the screen size setting
	BGCNT[0] = ((2 << 0) | (0 << 2) | (0 << 7) | (20 << 8) | (0 << 14)) ;
	BGCNT[1] = ((3 << 0) | (1 << 2) | (0 << 7) | (8 << 8) | (0 << 14)); 	//Looks at character block 1
	BGCNT[2] = ((0 << 0) | (0 << 2) | (0 << 7) | (19 << 8) | (0 << 14)) ;
	BGCNT[3] = ((1 << 0) | (0 << 2) | (0 << 7) | (18 << 8) |(0 << 14));
	
	//Pointer for character
	unsigned int* chars = (unsigned int*)0x6000000;
	
	//Blank character to go in first space of base block
	chars[0] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[1] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[2] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[3] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[4] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[5] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[6] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	chars[7] = ((0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0)|(0<<0));
	
	//Defines character base block location
	unsigned int* mychar = (unsigned int*)0x6010000;
	
	mychar[0] = 0;
	
	/*mychar[8] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(0<<20)|(0<<24)|(0<<28));
	
	mychar[9] = ((0<<0)|(0<<4)|(1<<8)|(1<<12)|(1<<16)|(1<<20)|(0<<24)|(0<<28));
	
	mychar[10] = ((0<<0)|(0<<4)|(1<<8)|(2<<12)|(2<<16)|(1<<20)|(0<<24)|(0<<28));
	
	mychar[11] = ((0<<0)|(0<<4)|(1<<8)|(2<<12)|(2<<16)|(1<<20)|(1<<24)|(0<<28));
	
	mychar[12] = ((0<<0)|(0<<4)|(1<<8)|(2<<12)|(2<<16)|(1<<20)|(1<<24)|(0<<28));
	
	mychar[13] = ((0<<0)|(0<<4)|(1<<8)|(2<<12)|(2<<16)|(1<<20)|(0<<24)|(0<<28));
	
	mychar[14] = ((0<<0)|(0<<4)|(1<<8)|(1<<12)|(1<<16)|(1<<20)|(0<<24)|(0<<28));
	
	mychar[15] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(0<<20)|(0<<24)|(0<<28));*/
	
	mychar[8] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(0<<20)|(0<<24)|(0<<28));
	
	mychar[9] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(0<<20)|(0<<24)|(0<<28));
	
	mychar[10] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(0<<20)|(0<<24)|(0<<28));
	
	mychar[11] = ((0<<0)|(0<<4)|(1<<8)|(1<<12)|(1<<16)|(1<<20)|(1<<24)|(1<<28));
	
	mychar[12] = ((0<<0)|(0<<4)|(1<<8)|(2<<12)|(2<<16)|(2<<20)|(1<<24)|(1<<28));
	
	mychar[13] = ((0<<0)|(0<<4)|(1<<8)|(1<<12)|(2<<16)|(1<<20)|(1<<24)|(1<<28));
	
	mychar[14] = ((0<<0)|(0<<4)|(1<<8)|(1<<12)|(2<<16)|(1<<20)|(1<<24)|(1<<28));
	
	mychar[15] = ((0<<0)|(0<<4)|(1<<8)|(1<<12)|(2<<16)|(1<<20)|(1<<24)|(1<<28));
	
	
	mychar[16] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(0<<20)|(0<<24)|(0<<28));
	
	mychar[17] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(0<<20)|(0<<24)|(0<<28));
	
	mychar[18] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(0<<20)|(0<<24)|(0<<28));
	
	mychar[19] = ((1<<0)|(1<<4)|(1<<8)|(1<<12)|(1<<16)|(0<<20)|(0<<24)|(0<<28));
	
	mychar[20] = ((1<<0)|(2<<4)|(2<<8)|(2<<12)|(1<<16)|(1<<20)|(0<<24)|(0<<28));
	
	mychar[21] = ((1<<0)|(1<<4)|(2<<8)|(1<<12)|(1<<16)|(1<<20)|(0<<24)|(0<<28));
	
	mychar[22] = ((1<<0)|(1<<4)|(2<<8)|(1<<12)|(1<<16)|(1<<20)|(0<<24)|(0<<28));
	
	mychar[23] = ((1<<0)|(1<<4)|(2<<8)|(1<<12)|(1<<16)|(1<<20)|(0<<24)|(0<<28));
	
	unsigned short* OAM = (unsigned short*)0x07000000;
	
	OAM[1 * 4] = ((50 << 0) | (0 << 9) | (0 << 14));
	OAM[(1 * 4) + 1] = ((50 << 0) | (0 << 9) | (1 << 14));
	OAM[(1 * 4) + 2] = ((1 << 0) | (0 << 10) | (0 << 12));
	
	//Palette colour definition
	unsigned short* palette = (unsigned short*)0x05000200;
	
	palette[0] = ((16 << 5) | (16 << 10));
	palette[1] = (16 << 0);
	
	//Defines position on screen to draw sprite
	//unsigned short* baseBlockAddress = (unsigned short*)0x6004000;
	//baseBlockAddress[5 * 32 + 5] = ((1 << 0) | (0 << 12));

	int count = 50;
	// main loop
	while (1) 
	{
		// ansi escape sequence to clear screen and home cursor
		// /x1b[line;columnH
		iprintf("\x1b[");

		// ansi escape sequence to set print co-ordinates
		// /x1b[line;columnH
		//iprintf("\x1b[0;0H %d", num);
		
	

	OAM[1 * 4] = ((count << 0) | (0 << 9) | (0 << 14));
	OAM[(1 * 4) + 1] = ((50 << 0) | (0 << 9) | (1 << 14));
	OAM[(1 * 4) + 2] = ((1 << 0) | (0 << 10) | (0 << 12));
	
		VBlankIntrWait();
		count++;
		
		if(count == 90){
			count = 50;
		}
	}
}


