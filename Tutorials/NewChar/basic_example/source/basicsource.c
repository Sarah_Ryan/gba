
#include <gba_console.h>
#include <gba_video.h>
#include <gba_interrupt.h>
#include <gba_systemcalls.h>

#include <stdio.h>

//---------------------------------------------------------------------------------
// Program entry point
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------

	// the vblank interrupt must be enabled for VBlankIntrWait() to work
	irqInit();
	irqEnable(IRQ_VBLANK);

	// generic setup function
	consoleDemoInit();
	
	unsigned int* sprite = (unsigned int*)0x06000860;
	
	sprite[0] = ((0<<0)|(0<<4)|(0<<8)|(1<<12)|(1<<16)|(1<<20)|(0<<24)|(1<<28));
	sprite[1] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(0<<16)|(1<<20)|(0<<24)|(1<<28));
	sprite[2] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(0<<24)|(1<<28));
	sprite[3] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(1<<24)|(1<<28));
	sprite[4] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(1<<24)|(1<<28));
	sprite[5] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(0<<24)|(1<<28));
	sprite[6] = ((0<<0)|(0<<4)|(0<<8)|(0<<12)|(1<<16)|(1<<20)|(0<<24)|(1<<28));
	sprite[7] = ((0<<0)|(0<<4)|(1<<8)|(1<<12)|(1<<16)|(1<<20)|(0<<24)|(0<<28));


	// main loop
	while (1) 
	{
		// ansi escape sequence to clear screen and home cursor
		// /x1b[line;columnH
		iprintf("\x1b[2J");

		// ansi escape sequence to set print co-ordinates
		// /x1b[line;columnH
		iprintf("\x1b[10;10HC");



	
		VBlankIntrWait();
	}
}


